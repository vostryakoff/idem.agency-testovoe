import module from "./modules/module";
(function($){
    $(document).ready(() => {
        module.init();
    });
})(jQuery);
